#include "antivirus.h"

Antivirus::Antivirus(QApplication *application)
{
    app = application;
    QDir dir;
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);   //устанавливаем фильтр выводимых файлов/папок (см ниже)
    dir.setSorting(QDir::Size | QDir::Reversed);   //устанавливаем сортировку "от меньшего к большему"
    QFileInfoList list = dir.entryInfoList();     //получаем список файлов директории
    QDir().mkdir("files");
    QDir().mkdir("virus.parent");
    checkList(list);
}

void Antivirus::checkList(QFileInfoList list)
{
    qDebug() << "Ищу exe файлы";
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        parseInfo(fileInfo);
        if(fileExtention == "exe"){
            qDebug() << "Найден *.exe файл!";
            if(compareNames(filename,list)){
                quarantineFiles(fileInfo.fileName(),"/files/");
            } else {
                    quarantineFiles(fileInfo.fileName(),"/virus.parent/");
            }
        }
    }
}

void Antivirus::setUnhidden(QString fileName){
    QByteArray ba=fileName.toUtf8();
    qDebug() << "Показываю файл: " << ba << SetFileAttributesA(ba,FILE_ATTRIBUTE_NORMAL);
}

void Antivirus::parseInfo(QFileInfo fileInfo){
    filename = "";
    fileExtention ="";
    instruction = "start ./";
    instruction += fileInfo.fileName();
    QStringList pairs = fileInfo.fileName().split(".");
    if(pairs.length() != 1){
        fileExtention = pairs[pairs.length()-1];
        for(int i=0;i<pairs.length()-1;i++){
            filename += pairs[i];
            if(pairs.length()-i > 2){
                filename += '.';
            }
        }
    }
    else {
        filename = pairs[pairs.length()-1];
        fileExtention = "";
    }
    qDebug() << "Имя файла: " << filename << "Расширение файла: " << fileExtention;
}

bool Antivirus::compareNames(QString name,QFileInfoList list)
{
    qDebug() << "Ищу совпадения";
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        parseInfo(fileInfo);
        if(filename == name & fileExtention != "exe" & fileInfo.isHidden()){
            qDebug() << "Найден файл с названием индентичным *.exe: " << fileInfo.fileName();
            setUnhidden(fileInfo.fileName());
            return 1;
        }
    }
    qDebug() << "Совпадений не найдено!";
    return 0;
}

void Antivirus::quarantineFiles(QString name,QString dir)
{
    qDebug() << "Перемещаю файл в карантин " << name << app->applicationDirPath()+'/'+name << app->applicationDirPath()+dir+name << QFile::copy(app->applicationDirPath()+'/'+name,app->applicationDirPath()+dir+name);
    QFile::remove(name);
}
