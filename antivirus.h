#ifndef ANTIVIRUS_H
#define ANTIVIRUS_H
#include <QApplication>
#include <windows.h>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <QDesktopServices>

class Antivirus
{
private:
    QApplication *app;
    QString filename,fileExtention,instruction;
public:
    Antivirus(QApplication *application);
    void checkList(QFileInfoList list);
    void parseInfo(QFileInfo fileInfo);
    void setUnhidden(QString fileName);
    bool compareNames(QString name, QFileInfoList list);
    void quarantineFiles(QString name,QString dir);
};

#endif // ANTIVIRUS_H
